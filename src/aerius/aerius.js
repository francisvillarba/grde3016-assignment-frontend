/**
 * aerius.js
 * 
 * The main core of the ãerius project.
 * Created for GRDE3016 - Internet Project Development, Curtin University,
 * Semester 2, 2020 by Francis Villarba for Assignment 3.
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 */

// Imports ------------------------------------------------------------------ /

import anime from 'animejs/lib/anime.es';

import { clock } from './applets/menuBar-clock';
import { handleUsernameMenuItem } from './applets/menuBar-user';
import { handleLogoutButton } from './applets/menuBar-logout';
import { handleConnectionStatusEvents } from './applets/menuBar-connection';

import { Launcher } from './apps/launcher';
import { handleLauncherButtonEvent } from './applets/menuBar-launcher';
import { WindowManagerInterface } from './wmi/wmi';

import { playStartupSound } from './apps/sounds';

import { registerUnavailableFunctions } from './apps/debug';

// Webpack Compatible Imports ----------------------------------------------- /
const loginTemplate = require ('../templates/login.mustache');
const loadingPanelTemplate = require('../templates/loadingPanel.mustache');

const menuBar = require('../templates/partials/menubar.mustache');

// Globals ------------------------------------------------------------------ /
const APP_ID = '#app';
const APP_BACKGROUND_ID = '#app-background';

// Functionality ------------------------------------------------------------ /

/**
 * Setup the login / register events and handlers
 */
function loginExperienceHandler() {

    // For debugging
    console.warn('Aerius', '-', 'Authentication required to proceed!');

    // Add a transition between the two account panels
    let signUpSwitch = document.querySelector('#register-panel-show');
    let signInSwitch = document.querySelector('#login-panel-show');

    signUpSwitch.addEventListener('click', () => {
        anime({
            targets: '#login-panel',
            opacity: 0,
            duration: 250,
            easing: 'easeOutQuad',
            begin: () => {
                document.querySelector('#register-panel').style.opacity = 0;
                document.querySelector('#register-panel').classList.remove('hidden');
                anime({
                    targets: '#register-panel',
                    opacity: 1,
                    duration: 250,
                    easing: 'easeOutQuad',
                });
            },
            complete: () => {
                document.querySelector('#login-panel').classList.add('hidden');
            }
        });
    });

    signInSwitch.addEventListener('click', () => {
        anime({
            targets: '#register-panel',
            opacity: 0,
            duration: 250,
            easing: 'easeOutQuad',
            begin: () => {
                document.querySelector('#login-panel').style.opacity = 0;
                document.querySelector('#login-panel').classList.remove('hidden');
                anime({
                    targets: '#login-panel',
                    opacity: 1,
                    duration: 250,
                    easing: 'easeOutQuad',
                });
            },
            complete: () => {
                document.querySelector('#register-panel').classList.add('hidden');
            }
        })
    });

    // Handle the login and register events
    let loginButton = document.querySelector('#login-form-submit');
    let registerButton = document.querySelector('#register-form-submit');

    loginButton.addEventListener('click', (event) => {
        event.preventDefault();
        // TODO - Future - Add the account system

        anime({
            targets: '#app',
            translateX: '100%',
            duration: 1000,
            easing: 'easeOutQuad',
            begin: () => {
                document.querySelector(APP_ID).style.transition = 'none';
            },
            complete: () => {
                document.querySelector('#login-panel').remove();
                document.querySelector('#register-panel').remove();
                document.querySelector(APP_ID).style = '';
                document.querySelector(APP_ID).style.transition = 'none';
                document.querySelector(APP_ID).style.cursor = 'wait';
                desktopExperienceInit();
            }
        });
    });

    registerButton.addEventListener('click', (event) => {
        event.preventDefault();
        // TODO - Future - Add the account system
        alert('The registration system is unavailable in this demo!');
    });
};

/**
 * Show the login screen for credentials etc.
 */
function loginExperienceInit() {
    // Add the login template to the page
    let loginTemplateOutput = loginTemplate();
    document.querySelector(APP_ID).insertAdjacentHTML('beforeend', loginTemplateOutput );

    // Handle the login events
    loginExperienceHandler();
};

/**
 * Show the desktop experience
 */
function desktopExperienceInit() {
    // For Debugging
    console.log('Aerius', '-', 'Preparing desktop environment...');

    // Add the loading panel template to the page
    let loadingPanelTemplateOutput = loadingPanelTemplate();
    document.querySelector(APP_ID).insertAdjacentHTML('beforeend', loadingPanelTemplateOutput);

    // Fade out the background
    anime({
        targets: APP_BACKGROUND_ID,
        opacity: 0,
        duration: 1000,
        delay: 150,
        complete: () => {
            anime({
                targets: APP_BACKGROUND_ID,
                opacity: 1,
                duration: 1000,
                delay: 1250,
                begin: () => {
                    document.querySelector(APP_BACKGROUND_ID).style = 'opacity: 0; background-image: url(static/demo_desktop_alt.jpg);';
                },
                complete: () => {
                    anime({
                        targets: '.panel-loader-container',
                        opacity: 0,
                        duration: 1000,
                        delay: 2500,
                        complete: () => {
                            document.querySelector('.panel-loader-container').remove();
                            document.querySelector(APP_ID).style = '';
                            desktopExperienceInitHelper();
                        }
                    });
                }
            });
        }
    });
};

/**
 * Helper function for the desktop experience demo
 */
function desktopExperienceInitHelper() {
    // For Debugging
    console.log('Aerius', '-', 'Desktop initialised!');

    // Show the menu bar
    let menuBarPartial = menuBar();
    document.querySelector(APP_ID).insertAdjacentHTML('beforeend', menuBarPartial);
    
    // TOOD - Init the helpers (and applets)
    clock();
    handleUsernameMenuItem();
    handleLogoutButton();
    handleConnectionStatusEvents();

    // Init the launcher and handlers
    Launcher.init();
    handleLauncherButtonEvent();
};

const Aerius = {

    init: () => {
        // TODO - Future - Remove desktopExperienceInit() and uncomment LoginExperienceInit();
        // loginExperienceInit();
        playStartupSound();
        desktopExperienceInit();
        WindowManagerInterface.init();

        // TODO - Remove on production build after testing is done
        setTimeout( () => {
            registerUnavailableFunctions();
        }, 8750);
    }
};

// Exports ------------------------------------------------------------------ /
export { Aerius };