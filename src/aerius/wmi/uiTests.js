/**
 * uiTests.js
 * Window Manager Interface - User Interface Tests Suite
 * These tests are designed for us to create test objects to ensure
 * the UI is functioning correctly (and bypass standard wmi scripts).
 * 
 * Mainly used to test if our mustache templates for windows are
 * functioning correclty before doing proper (and final implementation
 * of these windows and designs in the actual wmi scripts)
 * 
 * @author Francis Villarba <francis.villarba@me.com>
 * @version 1.0a
 */

// Constants ---------------------------------------------------------------- /

const APP_ID = '#app';
const defaultWindowTemplate = require('../../templates/windows/default_window.mustache');

const experimentsPartial = require('../../templates/partials/experiments-content.mustache');

// Globals ------------------------------------------------------------------ /
let initialMouseX;
let initialMouseY;

// Declare some values that we need
let currentTranslateX;
let currentTranslateY;

// Functionality ------------------------------------------------------------ /

export function openDefaultWindow(id) {
    // For debugging
    console.log('wmi/uiTests', '-', 'Opening Default Window');
    console.log('wmi/uiTests', '- id:', id);

    let tempWindow = defaultWindowTemplate({
        windowID: id,
        windowTitle: 'Default Window',
        canRollup: true
    });
    document.querySelector(APP_ID).insertAdjacentHTML('beforeend', tempWindow);
    
    setupDragEventHandlers(id);
    addExperiments(id);
}

function addExperiments(id) {
    let titleBarTextElement = document.querySelector(`[id='${id}']>.window-titlebar>.titlebar-center>.titlebar-item.titlebar-title`);
    let windowContent = document.querySelector(`[id='${id}']>.window-content`);

    console.log(titleBarTextElement);
    titleBarTextElement.innerText = 'Experiments';

    let experimentsBodyContent = experimentsPartial();
    windowContent.insertAdjacentHTML('beforeend', experimentsBodyContent);
}

function setupDragEventHandlers(id) {
    // For debugging
    console.log('wmi/uiTests', '-', 'Adding debug window drag handler');

    currentTranslateX = 0;
    currentTranslateY = 0;

    let titleBarElement = document.querySelector(`[id='${id}']>.window-titlebar`);
    let windowElement = document.querySelector(`#${id}`);

    titleBarElement.addEventListener('dragstart', (event) => {
        dragStartHandler(event, windowElement);
    }, false);

    titleBarElement.addEventListener('drag', (event) => {
        dragHandler(event, windowElement);
    }, false);

    titleBarElement.addEventListener('dragend', (event) => {
        dragEndHandler(event, windowElement);
    }, false);

    /// Webkit requires this to function correclty (without it, this will silently fail)
    titleBarElement.addEventListener('dragover', (event) => {
        dragEndHandler(event, windowElement);
    }, false);
}

function dragHandler(event, windowElement) {
    // For debugging
    console.log('Dragging', currentTranslateX, currentTranslateY);

    let transform =
    'translateX(' + (currentTranslateX - initialMouseX + event.clientX) + 'px)' +
    'translateY(' + (currentTranslateY - initialMouseY + event.clientY) + 'px)';

    // Using style.transform will use 2D graphics GPU instead of standard position variables
    windowElement.style.transform = transform;
}

function dragStartHandler(event, windowElement, currentTranslateX, currentTranslateY) {

    // What to do when we start dragging the window (initially)
    event.dataTransfer.effectAllowed = "copyMove";

    // Get the original mouse co-ordinates
    initialMouseX = event.clientX;
    initialMouseY = event.clientY;

    windowElement.style.opacity = '0.4';

    // Get the initial transformation vector and pull it out
    let currentTransform = window.getComputedStyle(windowElement).getPropertyValue('transform');
    console.log('Current Transformations', currentTransform);

    if( currentTransform === null || currentTransform === 'none' ) {
        // The transform has not occured initially
        currentTranslateX = 0;
        currentTranslateY = 0;
    } else {
        // A transform has occured previously
        currentTransform = currentTransform.match(/(-?[0-9\.]+)/g);

        // Set the transform variables
        currentTranslateX = currentTransform[4];
        currentTranslateY = currentTransform[5];

        // For Debugging
        console.log('currentX', currentTranslateX);
        console.log('currentY', currentTranslateY);
    }
};

function dragEndHandler(event, windowElement) {
    event.preventDefault();
    event.dataTransfer.dropEffect = "move";

    // For debugging
    console.log("Initial Mouse Co-ordinates", initialMouseX, initialMouseY);
    console.log("Final mouseX: ", event.clientX, windowElement.offsetWidth);
    console.log("Final mouseY: ", event.clientY, windowElement.offsetHeight);

    // Ensure the final position is set
    windowElement.style.opacity = '1';
};