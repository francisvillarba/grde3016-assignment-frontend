/**
 * aerius/wmi/window.js
 * 
 * The inner core of Aerius' Window Management Interface.
 * This file defines the window schema (via classes) as well as the various
 * features and functionality that a window can have.
 * 
 * @author Francis Villarba <francis.villarba@me.com> 
 */

// Constants ---------------------------------------------------------------- /
const DEFAULT_WINDOW_WIDTH = '800';
const DEFAULT_WINDOW_HEIGHT = '600';

// Functionality ------------------------------------------------------------ /

class Window {
    /**
     * The constructor for the Window Object
     * @param {String} title - The title of the Window / Applet
     * @param {Object} content - The content to display
     * @param {String} id - The UUID for the Window / Applet (For Query Selectors etc.)
     */
    constructor(title, content, id, width, height) {
        this.title = title;
        this.content = content;
        this.id = id;
        this.width = DEFAULT_WINDOW_WIDTH || width;
        this.height = DEFAULT_WINDOW_HEIGHT || height;
    }

    /**
     * Initialises the default Window Parameters
     */
    init() {
        // Open, Rolled-up, Minimize, Maximize, Exclusive Full-Screen
        this.state = 1;
        this.canResize = true;
        this.canRollup = true;
    }

    /**
     * Set's the window's position
     * @param {Number} x - The x position 
     * @param {Number} y - The y position
     */
    setPosition(x, y) {
        this.posX = x;
        this.posY = y;
    }

    setWidth( width ) {
        this.width = width;
    }

    setHeight( height ) {
        this.height = height;
    }

    // Getters for various variables for the window ------------------------- /
    getPosX() {
        return this.posX;
    }

    getPosY() {
        return this.posY;
    }
}

// Exports ------------------------------------------------------------------ /
export { Window };