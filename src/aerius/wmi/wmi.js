/**
 * aerius/wmi/wmi.js
 * The window management interface system component for the Aerius Desktop Environment
 * 
 * @author Francis Villarba <francis.villarba@me.com>
 */

// Imports ------------------------------------------------------------------ /

import { Window } from './window';
import { playNotificationSound, playExclamationSound } from '../apps/sounds';

// Constants ---------------------------------------------------------------- /
const APP_ID = '#app';

const DEFAULT_WINDOW_TEMPLATE = require('../../templates/windows/default_window.mustache');
const DEFAULT_NOTIFICATION_WINDOW_TEMPLATE = require('../../templates/windows/default_notification_window.mustache');

// Functionality ------------------------------------------------------------ /

//== Window Moving Functionality =============================================/

/**
 * Makes an element draggable with a mouse cursor
 * Inspired by https://www.w3schools.com/howto/howto_js_draggable.asp but adapted to work with Aerius
 * @param {String} id - The Window to add the drag functionality to
 */
function makeElementDraggable(id) {
    let pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    let elmnt = document.getElementById(id);
    
    elmnt.onmousedown = dragMouseDown;

    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        // call a function whenever the cursor moves:
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        // calculate the new cursor position:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;

        // set the element's new position:
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    }

    function closeDragElement() {
        /* stop moving when mouse button is released:*/
        document.onmouseup = null;
        document.onmousemove = null;
    }
};

/**
 * The alternative method for making an element draggable with a mouse cursor
 * This version deals specifically with touch events
 * @param {String} id - The window to add the drag functionality to
 */
function makeElementTouchDraggable(id) {
    let pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    let elmnt = document.getElementById(id);

    elmnt.ontouchstart = touchMove;

    function touchMove(e) {
        e = e || window.event;
        e.preventDefault();

        e.touches[0];
        pos3 = e.pageX;
        pos4 = e.pageY;
        document.ontouchend = closeTouchMoveElement;
        document.ontouchmove = touchDrag;
    }

    function touchDrag(e) {
        e = e || window.event;
        e.preventDefault();

        pos1 = pos3 - e.pageX;
        pos2 = pos4 - e.pageY;
        pos3 = e.pageX;
        pos4 = e.pageY;

        // set the element's new position:
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    }

    function closeTouchMoveElement() {
        document.ontouchend = null;
        document.ontouchmove = null;
    }
}

//== Window Observer Functionality ===========================================/

/**
 * The core window observer functionality that augments the functionality
 * of WMI. The role of big brother is to observe all mouse click events on
 * the page and determine what function to call, based on the element selected.
 */
function bigBrother() {
    // When any part of the document is clicked
    document.onclick = (event) => {
        // For debugging
        // console.debug('Aerius', 'WMI Observer - Big Brother', event.target);

        let clickTarget = event.target;

        // Determine what was clicked, based on the classes
        if( clickTarget.classList.contains('window-close') ) {
            // For debugging
            // console.debug('Close button was clicked!');
            WindowManagerInterface.closeWindow( clickTarget.closest('.window-container').id );
        }

        if( clickTarget.classList.contains('window-fullscreen') ) {
            // For debugging
            console.debug('Window fullscreen toggle button was clicked!');
            toggleWindowFullScreen( clickTarget.closest('.window-container').id );
        }

        if( clickTarget.classList.contains('window-rollup') ) {
            // For debugging
            console.debug('Window rollup toggle button was clicked!');
        }

        if( clickTarget.classList.contains('window-titlebar') || clickTarget.classList.contains('window-content') ) {
            // For debugging
            // console.debug('A window was selected!', `ID = ${clickTarget.closest('.window-container').id}`);
            toggleActiveWindow(clickTarget.closest('.window-container').id);
        }

        if( clickTarget.hasAttribute('close-notification') ) {
            WindowManagerInterface.closeWindow( clickTarget.closest('.window-container').id );
        }
    }

    // When any part of the document is tapped (using touchscreen)
    document.ontouchstart = (event) => {
        // For debugging
        // console.debug('Aerius', 'WMI Observer - Big Brother', event.targetTouches[0].target);

        let touchTarget = event.targetTouches[0].target;

        // Determine what was clicked, based on the classes
        if (touchTarget.classList.contains('window-close')) {
            // For debugging
            // console.debug('Close button was clicked!');
            WindowManagerInterface.closeWindow(touchTarget.closest('.window-container').id);
        }

        if (touchTarget.classList.contains('window-fullscreen')) {
            // For debugging
            console.debug('Window fullscreen toggle button was clicked!');
            toggleWindowFullScreen( touchTarget.closest('.window-container').id );
        }

        if (touchTarget.classList.contains('window-rollup')) {
            // For debugging
            console.debug('Window rollup toggle button was clicked!');
        }

        if (touchTarget.classList.contains('window-titlebar') || touchTarget.classList.contains('window-content')) {
            // For debugging
            // console.debug('A window was selected!', `ID = ${touchTarget.closest('.window-container').id}`);
            toggleActiveWindow(touchTarget.closest('.window-container').id);
        }
        
        if (touchTarget.hasAttribute('close-notification') ) {
            WindowManagerInterface.closeWindow( touchTarget.closest('.window-container').id );
        }
    }
}

//== Window Titlebar Functionality ===========================================/

/**
 * Sets the active window to be the selected one
 * @param {String} id - The ID of the window
 */
function toggleActiveWindow(id) {
    // For debugging
    // console.debug('toggleActiveWindow', `Target is = ${id}`);

    let targetWindow = document.querySelector(`div[id='${id}']`);

    if( targetWindow.classList.contains('window-inactive')) {
        // Get the current active window
        let currentActiveWindow = document.querySelector(`div[id='${WindowManagerInterface.getActiveWindow()}']`);

        // For debugging
        // console.debug('Soon to be previously active window', currentActiveWindow);

        // Make the current active window inactive
        currentActiveWindow.classList.add('window-inactive');
        // Make our target window the active window
        targetWindow.classList.remove('window-inactive');
        WindowManagerInterface.setActiveWindow(id);
    }
}

//== Window Full Screen Functionality ======================================= /

/**
 * Toggles the window from full screen to normal view
 */
function toggleWindowFullScreen(id) {
    let targetWindow = document.querySelector(`div[id='${id}']`);
    
    if( targetWindow.classList.contains('window-is-fullscreen')) {
        // If it is already full screen, toggle full screen class
        targetWindow.querySelector(':scope > .window-titlebar').setAttribute('draggable', true);
        targetWindow.classList.remove('window-is-fullscreen');
        targetWindow.style.height = WindowManagerInterface.getWindowData(id - 1).height;
        targetWindow.style.width = WindowManagerInterface.getWindowData(id - 1).width;
    } else {
        // If it is currently not full screen

        // Save the current width and height values
        WindowManagerInterface.windowData[id - 1].setHeight( targetWindow.style.height );
        WindowManagerInterface.windowData[id - 1].setWidth( targetWindow.style.width );

        // Set the full-screen params
        targetWindow.querySelector(':scope > .window-titlebar').setAttribute('draggable', false);
        targetWindow.classList.add('window-is-fullscreen');

        // Clear the previous styles
        targetWindow.style.removeProperty('height');
        targetWindow.style.removeProperty('width');
    }
}


const WindowManagerInterface = {
    // References / Selector IDs of windows currently open
    windows: [],
    // Configuration for a particular window
    windowData: [],
    // What is the currently active window
    activeWindow: null,

    /**
     * Initialise the Window Management Interface
     */
    init: () => {
        console.log('Aerius', '-', 'Initialising the Window Management Interface');
        WindowManagerInterface.windows = [];
        bigBrother();
    },

    /**
     * Get the currently active window
     * @return {Number} - The active window as an index of the windows array
     */
    getActiveWindow() {
        return WindowManagerInterface.activeWindow;
    },

    /**
     * Get the currently active window's selector ID
     */
    getActiveWindowId() {
        if( WindowManagerInterface.windows.length != 0 ) {
            return WindowManagerInterface.windows[WindowManagerInterface.activeWindow];
        } else {
            // console.log('Aerius', 'Window Manager Interface', 'There is currently no windows open!');
            return null;
        }
    },

    /**
     * Set the currently active window reference to be the new active window
     */
    setActiveWindow(id) {
        // For debugging
        // console.debug('Active Window Set To:', id);

        if( WindowManagerInterface.windows.length >= id ) {
            WindowManagerInterface.activeWindow = id;
        } else {
            // console.error('Aerius', 'Window Manager Interface', `${id} is not a valid window reference!`);
        }
    },

    /**
     * Get the selected window's data
     * @param {Number} id - The reference to the window
     */
    getWindowData(id) {
        if( WindowManagerInterface.windows.length != 0 ) {
            return WindowManagerInterface.windowData[id];
        } else {
            // console.log('Aerius', 'Window Manager Interface', 'There is currently no windows open!');
            return null;
        }
    },

    /**
     * Closes a window and cleans up the arrays
     */
    closeWindow: (id) => {
        // For debugging
        // console.debug('Aerius', 'WindowManagerInterface', 'Closing Window', id);

        // Check if the window is in bounds first
        if( WindowManagerInterface.windows.includes(id) ) {
            let currentActiveWindow = WindowManagerInterface.activeWindow;

            // For debugging
            // console.debug('closeWindow()', 'Current Active Window is: ', currentActiveWindow);
            // console.debug('closeWindow()', 'Length of windows array: ', WindowManagerInterface.windows.length);

            // Make the window active if it is not already
            if( currentActiveWindow != id ) {
                toggleActiveWindow(id);
            }

            if (WindowManagerInterface.windows.length > 1) {
                let windowIndex = WindowManagerInterface.windows.indexOf(id);
                // If there is still some windows left after this, splice the array
                WindowManagerInterface.windows.splice(windowIndex, 1);
                WindowManagerInterface.windowData.splice(windowIndex, 1);
                // Set the current active window to this window (length - 1 as arrays start from zero)
                WindowManagerInterface.activeWindow = WindowManagerInterface.windows[WindowManagerInterface.windows.length - 1];

                //For debugging
                // console.debug('closeWindow()', 'New active window is: ', WindowManagerInterface.activeWindow);
                // console.debug('closeWindow()', 'New Length of windows array: ', WindowManagerInterface.windows.length);

                // Force set the new active window as the active window
                document.querySelector(`div[id='${WindowManagerInterface.activeWindow}']`).classList.remove('window-inactive');

            } else {
                WindowManagerInterface.activeWindow = null;
                WindowManagerInterface.windows = [];
                WindowManagerInterface.windowData = [];

                // For debugging
                // console.debug('Aerius', 'WindowManagerInterface', 'Windows and WindowsData is now empty');
            }
            // Remove the window
            document.querySelector(`[id='${id}']`).remove();
        }
    },

    /**
     * Opens a default window and appends it to the desktop
     */
    openDefaultWindow: (title, content) => {
        // Create the window
        let newWindowId = (WindowManagerInterface.windows.length + 1).toString();
        let newWindow = new Window( title, content, newWindowId );

        // Init the window's object fields
        newWindow.init();

        // Create the window from Mustache Template
        let newWindowUI = DEFAULT_WINDOW_TEMPLATE({
            windowID: newWindowId,
            windowTitle: title,
            canRollup: true
        });

        // Append the window to the DOM
        document.querySelector(APP_ID).insertAdjacentHTML('beforeend', newWindowUI);
        
        // Setup the window
        let newWindowDOM = document.querySelector(`div[id='${newWindowId}']>.window-content`);
        newWindowDOM.innerHTML = content;

        // Set any previous windows as inactive
        if( WindowManagerInterface.windows.length > 0 ) {
            // If the desktop already contains some windows
            document.querySelector(`div[id='${WindowManagerInterface.getActiveWindow()}']`).classList.add('window-inactive');
        }
        // Update the window manager
        WindowManagerInterface.windows.push(newWindowId);
        WindowManagerInterface.windowData.push(newWindow);

        // Set the active window to the newly created window
        WindowManagerInterface.setActiveWindow(newWindowId);

        // Make it movable (mouse + touch)
        makeElementDraggable(newWindowId);
        makeElementTouchDraggable(newWindowId);

        // For debugging
        // console.debug('Active Window', WindowManagerInterface.activeWindow);
        // console.debug('Windows Array', WindowManagerInterface.windows);
    },

    /**
     * Opens a notification window and appends it to the desktop
     */
    openNotificationWindow: (title, content) => {
        // Create the window
        let newWindowId = (WindowManagerInterface.windows.length + 1).toString();
        let newWindow = new Window( title, content, newWindowId );

        // Init the window's object fields
        newWindow.init();

        // Create the notification window from the Mustache Template
        let newNotificationWindowUI =  DEFAULT_NOTIFICATION_WINDOW_TEMPLATE({
            windowID: newWindowId,
            windowTitle: title,
            canRollup: false
        });

        // Append the window to the DOM
        document.querySelector(APP_ID).insertAdjacentHTML('beforeend', newNotificationWindowUI);

        // Play the notification sound
        playNotificationSound();

        // Setup the window
        let newWindowDOM = document.querySelector(`div[id='${newWindowId}']>.window-content`);
        newWindowDOM.innerHTML = content;

        // Set any previous windows as inactive
        if( WindowManagerInterface.windows.length > 0 ) {
            // If the desktop already contains some windows
            document.querySelector(`div[id='${WindowManagerInterface.getActiveWindow()}']`).classList.add('window-inactive');
        }
        // Update the window manager
        WindowManagerInterface.windows.push(newWindowId);
        WindowManagerInterface.windowData.push(newWindow);

        // Set the active window to the newly created window
        WindowManagerInterface.setActiveWindow(newWindowId);

        // Make it movable (mouse + touch)
        makeElementDraggable(newWindowId);
        makeElementTouchDraggable(newWindowId);
    },
}

// Exports ------------------------------------------------------------------ /
export { WindowManagerInterface };