/**
 * aerius/wmi/debug.js
 * 
 * Debug tools for the Window Management Interface
 * @author Francis Villarba <francis.villarba@me.com>
 */

import { WindowManagerInterface } from './wmi';

export function openTestWindow() {
    let windowContent = 'Hello World';
    setTimeout( () => {
        WindowManagerInterface.openDefaultWindow('Debug Window Test', windowContent);
        setTimeout(() => {
            WindowManagerInterface.openDefaultWindow('Secondary Window', 'This is a secondary window');
        }, 1500);
    }, 7000);
}