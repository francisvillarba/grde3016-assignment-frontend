/**
 * aerius/apps/stackedit
 * The Stackedit - Markdown Editor applet for the Aerius Desktop Experience
 * @author Francis Villarba <francis.villarba@me.com>
 */

/* Constants ---------------------------------------------------------------- */

import { Launcher, AppTile } from './launcher';
import { WindowManagerInterface } from '../wmi/wmi';

const STACKEDIT_APP_CONTENT = require('../../templates/apps/stackedit.mustache');

/* App Functionality ------------------------------------------------------- */

export const StackEdit = {
        /**
     * Initialises the app
     */
    init: () => {
        StackEdit.addToLauncher();
    },

        /**
     * Adds the codesandbox applet to the Launcher
     */
    addToLauncher: () => {
        let callback = () => {
            StackEdit.launch();
        }

        let stackedit_tile = new AppTile(
            'StackEdit',
            'static/apps/stackedit.png',
            'static/apps/stackedit.png',
            callback
        );

        Launcher.registerApplet(stackedit_tile);
    },

    /**
     * The launch logic for the codesandbox applet
     */
    launch: () => {
        WindowManagerInterface.openDefaultWindow('StackEdit', STACKEDIT_APP_CONTENT());

        // Set the appropriate size for the winodw
        let appID = WindowManagerInterface.getActiveWindow();
        document.querySelector(`div[id='${appID}']`).style.height = '800px';
        document.querySelector(`div[id='${appID}']`).style.width = '1280px';
    }
}