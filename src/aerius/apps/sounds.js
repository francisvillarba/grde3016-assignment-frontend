/**
 * aerius/apps/sounds.js
 * 
 * Used to play system sounds in the Aerius Desktop Experience
 * @author Francis Villarba <francis.villarba@me.com>
 */

// Constants --------------------------------------------------------------- */
const STARTUP_AUDIO_SOUND_ID = '#startup-sound';
const NOTIFICATION_AUDIO_SOUND_ID = "#notification-sound";
const EXCLAMATION_AUDIO_SOUND_ID = '#exclamation-sound';

// Functionality ----------------------------------------------------------- */

export function playStartupSound() {
    // TODO - Do a proper promise as play returns a promise, so that after play, it resets the player
    try {
        document.querySelector(STARTUP_AUDIO_SOUND_ID).play();
    } catch( err ) {
        console.err('Unable to play startup sound', 'Media autoplay is likely disabled');
        console.log(err);
    }
}

export function playNotificationSound() {
    // TODO - Do a proper promise as play returns a promise, so that after play, it resets the player
    try {
        document.querySelector(NOTIFICATION_AUDIO_SOUND_ID).play();
    } catch (err) {
        console.err('Unable to play notification sound', 'Media autoplay is likely disabled');
        console.log(err);
    }
}

export function playExclamationSound() {
    // TODO - Do a proper promise as play returns a promise, so that after play, it resets the player
    try {
        document.querySelector(EXCLAMATION_AUDIO_SOUND_ID).play();
    } catch (err) {
        console.err('Unable to play exclamation sound', 'Media autoplay is likely disabled');
        console.log(err);
    }
}
