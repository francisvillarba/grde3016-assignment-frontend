/**
 * launcher.js
 * The application / applet launcher for Aerius. Expanded from the 
 * experiment "applet-launcher-ui" and "applet-launcher-ui-v2".
 * @author Francis Villarba <francis.villarba@me.com>
 * @version 1.1c
 */

// Imports ------------------------------------------------------------------ /
import VanillaTilt from 'vanilla-tilt';

import { openLancher, closeLauncher } from '../applets/menuBar-launcher';

// Constants ---------------------------------------------------------------- /

const launcherTemplate = require('../../templates/partials/launcher.mustache');

const appTileTemplate = require('../../templates/items/launcher_appTile.mustache');
const recentAppTileTemplate = require('../../templates/items/launcher_recent-appTile.mustache');

// For debugging only
// TODO - Remove this when it is not required naymore
// import { openDefaultWindow } from '../wmi/uiTests';

// Apps List ---------------------------------------------------------------- /

// TODO - Future - Have a seperate app management applet to handle these (to make launcher.js much more focused)

import { Codesandbox } from './codesandbox';
import { Fonts_Manager } from './fonts-manager';
import { Icons_Gallery } from './icons-gallery';
import { Portfolio } from './portfolio';
import { Spotify_WebApp } from './spotify';
import { Unsplash } from './unsplash';
import { StackEdit } from './stackedit';

// Globals ------------------------------------------------------------------ /
const APP_ID = '#app';
const LAUNCHER_ID = "#launcher-ui";
const NO_RECENT_NOTICE_ID = "#no-recent-message";
const ALL_APPS_GRID_ID = '#all-apps-grid';
const RECENT_APPS_LIST_ID = '#recent-apps-list';

// Functionality ------------------------------------------------------------ /

/**
 * Represents a launcher application tile
 * @constructor
 * @param {string} title - The title of the applet / application
 * @param {string} icon - The src of the icon
 * @param {string} image - The src of the full res image of the applet
 * @param {Object} callback - The callback to launch the applet / application
 */
class AppTile {
    constructor( title, icon, image, callback ) {
        this.title = title;
        this.icon = icon;
        this.image = image;
        this.callback = callback;
    };

    /**
     * Launch the associated applet / application
     */
    launch() {
        this.callback();
    }

    getTitle() {
        return this.title;
    }

    getIcon() {
        return this.icon;
    }

    getImage() {
        return this.image;
    }
};

/**
 * Adds the launcher UI to the page
 */
function addLauncherToPage() {
    // Add the launcher to the page (initially)
    let appTarget = document.querySelector(APP_ID);
    let launcherUI = launcherTemplate();
    appTarget.insertAdjacentHTML('beforeend', launcherUI);

    // TODO: Show the no-recent apps text; Uncomment when this feature is implemented
    // document.querySelector(NO_RECENT_NOTICE_ID).classList.remove('hidden');

    // For debugging only
    // addDebugApps();

    initVanillaTilt();
};

/**
 * Initialise Vanilla Tilt on the app-tile elements
 */
function initVanillaTilt() {
    VanillaTilt.init( document.querySelectorAll('.app-tile'));
};

/**
 * Initialises other applet tiles so they can be added to the applet launcher
 * User Interface.
 */
function initApplets() {
    Codesandbox.init();
    Fonts_Manager.init();
    Icons_Gallery.init();
    Portfolio.init();
    Spotify_WebApp.init();
    StackEdit.init();
    Unsplash.init();
}

/**
 * DEBUGGING ONLY - Add test launcher apps
 */
function addDebugApps() {
    let allAppsTarget = document.querySelector(ALL_APPS_GRID_ID);

    // A loop to make some sample applets
    for( let i = 0; i < 12; i++ ) {
        let appTileTemp = appTileTemplate({
            appTileID: `tile${i}`,
            appTitle: `Test tile${i}`
        });
        allAppsTarget.insertAdjacentHTML('beforeend', appTileTemp);

        if( i == 0 ) {
            document.querySelector(`#tile${i}`).addEventListener('click', () => {
                openDefaultWindow(`window${i}`);
                closeLauncher();
            });
        } else {
            document.querySelector(`#tile${i}`).addEventListener('click', () => {
                alert(`This would launch applet title: tile${i}`);
            });
        }
    }
}

/**
 * The launcher itself
 */
const Launcher = {
    recentApps: [],
    allApps: [],

    /**
     * Initialises the launcher
     */
    init() {
        Launcher.recentApps = [];
        Launcher.allApps = [];

        // Append the launcher to the dom (but keep it hidden until needed)
        addLauncherToPage();

        // Init third party / extention applets
        initApplets();
    },

    /**
     * Add an application to the recent apps list
     * @param {appTile} appTile - The application tile
     */
    addRecent( appTile ) {
        // If there is currently 3 items in the recents list
        if( Launcher.recentApps.length = 3) {
            Launcher.removeRecent(3);
        }

        // Add this applet to the front of the array
        Launcher.recentApps = [appTile].concat(Launcher.recentApps);
    },

    /**
     * Remove an application from the recent apps list
     * @param {number} index - The index of the item to remove
     */
    removeRecent( index ) {
        if( Launcher.recentApps.length != 0 && Launcher.recentApps.length > 1 ) {
            Launcher.recentApps.splice( index, 1 );
        } else {
            Launcher.recentApps = [];
        }
    },

    /**
     * Clears the list of recent applications
     */
    clearRecents() {
        Launcher.recentApps = [];
    },

    /**
     * Shows the application launcher
     */
    show() {
        // For debugging
        // console.log('Launcher', '-', 'Now showing the applet launcher');
        
        document.querySelector(LAUNCHER_ID).classList.remove('hidden');
        document.querySelector(LAUNCHER_ID).classList.add('animate-open');
    },

    /**
     * Hides the application launcher
     */
    hide() {
        // For debugging
        // console.log('Launcher', '-', 'Now hiding the applet launcher');

        document.querySelector(LAUNCHER_ID).classList.remove('animate-open');
        document.querySelector(LAUNCHER_ID).classList.add('animate-close');
        setTimeout( () => {
            document.querySelector(LAUNCHER_ID).classList.add('hidden');
            document.querySelector(LAUNCHER_ID).classList.remove('animate-close');
        }, 250);
    },

    /**
     * Launches a particular applet / application then adds it to the recents list
     * @param {number} index - The index of the applet / application to launch
     */
    launch(index) {
        // TODO - Future, check if this is a valid and in bound applet
        Launcher[index].launch();
    },

    /**
     * Adds an applet / widget to the Launcher
     * @param {AppTile} appTile - The application tile object
     */
    registerApplet( appTile ) {
        let allAppsTarget = document.querySelector(ALL_APPS_GRID_ID);
        let appNumber = Launcher.allApps.length + 1;
        let tempTileId = `tile-${appNumber}`;

        // For debugging
        // console.debug(tempTileId, appNumber);

        let appTileTemp = appTileTemplate({
            appTileID: tempTileId,
            appTitle: `${appTile.title}`
        });
        allAppsTarget.insertAdjacentHTML('beforeend', appTileTemp);

        // Style the app tile
        let appTileReference = document.querySelector(`#${tempTileId}`);
        appTileReference.style.backgroundImage = `url('${appTile.getIcon()}')`;
        appTileReference.style.backgroundSize = 'cover';
        appTileReference.style.backgroundColor = 'white';
        appTileReference.style.backgroundPosition = 'top';
        appTileReference.style.backgroundRepeat = 'no-repeat';

        // Style the app's text
        let appTileTitleReference = document.querySelector(`#${tempTileId} > .app-tile-title`);
        appTileTitleReference.style.backgroundColor = 'rgba(0,0,0,0.75)';
        appTileTitleReference.style.padding = '12px';

        // Add event listeners
        appTileReference.addEventListener( 'click', () => {
            closeLauncher();
            appTile.launch();
        });

        // Append the tile reference to the Launcher All Apps Array
        Launcher.allApps.push( tempTileId );
    }
};

// Exports ------------------------------------------------------------------ /
export { Launcher, AppTile };