/**
 * aerius/apps/codesandbox.io
 * The codesandbox applet for the Aerius Desktop Experience
 * @author Francis Villarba <francis.villarba@me.com>
 */

/* Constants ---------------------------------------------------------------- */

import { Launcher, AppTile } from './launcher';
import { WindowManagerInterface } from '../wmi/wmi';

const CODESANDBOX_APP_CONTENT = require('../../templates/apps/codesandbox.mustache');

/* App Functionality ------------------------------------------------------- */

export const Codesandbox = {
    /**
     * Initialises the app
     */
    init: () => {
        Codesandbox.addToLauncher();
    },

    /**
     * Adds the codesandbox applet to the Launcher
     */
    addToLauncher: () => {
        let callback = () => {
            Codesandbox.launch();
        }

        let codesandbox_tile = new AppTile(
            'Codesandbox',
            'static/apps/codesandbox.png',
            'static/apps/codesandbox.png',
            callback
        );

        Launcher.registerApplet(codesandbox_tile);
    },

    /**
     * The launch logic for the codesandbox applet
     */
    launch: () => {
        WindowManagerInterface.openDefaultWindow('Codesandbox', CODESANDBOX_APP_CONTENT());

        // Set the appropriate size for the winodw
        let appID = WindowManagerInterface.getActiveWindow();
        document.querySelector(`div[id='${appID}']`).style.height = '800px';
        document.querySelector(`div[id='${appID}']`).style.width = '1280px';
    }
}