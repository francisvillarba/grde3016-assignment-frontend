/**
 * aerius/apps/notiifcation.js
 * The notification system application for the Aerius Desktop Experience
 * @author Francis Villarba <francis.villarba@me.com>
 */

// Imports ------------------------------------------------------------------ /

// Constants ---------------------------------------------------------------- /

// Functionality ------------------------------------------------------------ /

// Exports ------------------------------------------------------------------ /