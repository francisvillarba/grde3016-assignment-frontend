/**
 * aerius/apps/spotify.js
 * The spotify applet for the Aerius Desktop Experience
 * @author Francis Villarba <francis.villarba@me.com>
 */

/* Constants ---------------------------------------------------------------- */

import { Launcher, AppTile } from './launcher';
import { WindowManagerInterface } from '../wmi/wmi';

const SPOTIFY_PLAYER_APP_CONTENT = require('../../templates/apps/spotify.mustache');

/* App Functionality ------------------------------------------------------- */

export const Spotify_WebApp = {
    /**
     * Initialises the app / feature
     */
    init: () => {
        Spotify_WebApp.addToLauncher();
    },

    /**
     * Adds the spotify webapp applet to the Launcher
     */
    addToLauncher: () => {
        let callback = () => {
            Spotify_WebApp.launch();
        }

        let spotify_webapp_tile = new AppTile(
            'Spotify',
            'static/apps/spotify.png',
            'static/apps/spotify.png',
            callback
        );

        Launcher.registerApplet(spotify_webapp_tile);
    },

    /**
     * The core launcher logic for the spotify webapp applet
     */
    launch: () => {
        WindowManagerInterface.openDefaultWindow('Spotify Web Player', SPOTIFY_PLAYER_APP_CONTENT());
    }
};