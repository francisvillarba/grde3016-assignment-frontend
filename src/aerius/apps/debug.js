/**
 * aerius/apps/debug.js
 * 
 * The debugging application suites for Aerius Desktop that faciliate
 * and help with the creation, development and testing of various
 * aspects of the Aerius Desktop Experience.
 * 
 * @author Francis Villarba <francis.villarba@me.com>
 */

// Imports ------------------------------------------------------------------ /

import { WindowManagerInterface } from '../wmi/wmi';

// Constants ---------------------------------------------------------------- /

const NOTIFICATION_BODY_TEMPLATE = require('../../templates/partials/notification-body.mustache');

// Globals ------------------------------------------------------------------ /

const NOT_IMPLEMENTED_BODY_COPY = "This feature has not been implemented yet! Please check back later!";

// Functionality ------------------------------------------------------------ /

/**
 * Helper function for registerUnavailableFunctions that shows a notification 
 * message to the user that a particular feature is not currently available
 */
function showUnavailableNotice() {
    let notificationBody = NOTIFICATION_BODY_TEMPLATE({
        bodyText: NOT_IMPLEMENTED_BODY_COPY
    });

    WindowManagerInterface.openNotificationWindow('Notice', notificationBody);
}

/**
 * Registers event listeners for functions that are not currently available
 * in this build yet
 */
export function registerUnavailableFunctions() {
    console.log('Aerius', '-', 'Debug Applet', '-', 'Registering Unavailable Functions Notice');

    // Running apps list
    document.querySelector('#app-running-list-button').addEventListener('click', () => {
        showUnavailableNotice();
    });

    // The launcher search field
    document.querySelector('.launcher-header-search').addEventListener('click', () => {
        showUnavailableNotice();
    })
}