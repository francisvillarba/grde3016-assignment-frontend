/**
 * aerius/apps/portfolio.js
 * 
 * My own personal portfolio website, as an applet, to demonstrate
 * other websites being usable within the Aerius Desktop Environment.
 * 
 * This is also some shameless plug for my own personal portfolio
 * website as well, if I am being honest :P
 * 
 * @author Francis Villarba <francis.villarba@me.com>
 */

/* Constants ---------------------------------------------------------------- */

import { Launcher, AppTile } from './launcher';
import { WindowManagerInterface } from '../wmi/wmi';

const PORTFOLIO_APP_CONTENT = require('../../templates/apps/portfolio.mustache');

/* App Functionality ------------------------------------------------------- */

export const Portfolio = {
    /**
     * Initialises the app / feature
     */
    init: () => {
        Portfolio.addToLauncher();
    },

    /**
     * Adds the Portfolio Applet to the Launcher
     */
    addToLauncher: () => {
        let callback = () => {
            Portfolio.launch();
        }

        let portfolioTile = new AppTile(
            'Personal Portfolio',
            'static/apps/portfolio.png',
            'static/apps/portfolio.png',
            callback
        );

        Launcher.registerApplet(portfolioTile);
    },

    /**
     * Launch the Applet
     */
    launch: () => {
        // Launch the Portfolio Website Applet
        WindowManagerInterface.openDefaultWindow('Personal Portfolio', PORTFOLIO_APP_CONTENT());

        // Resize the window to make it larger
        let windowId = WindowManagerInterface.getActiveWindow();
        document.querySelector(`div[id='${windowId}']`).style.height = '800px';
    }
};