/**
 * aerius/apps/icons-gallery.js
 * This application allows you to browse through icons that will be used in
 * future features / custom applications in the Aerius Project.
 * 
 * This was primarily added to satisfy the assessment requirements for 
 * Curtin University, GRDE3016, Internet Project Development, Assessment 3.
 * 
 * @author Francis Villarba <francis.villarba@me.com>
 */

/* Constants ---------------------------------------------------------------- */

import { Launcher, AppTile } from './launcher';
import { WindowManagerInterface } from '../wmi/wmi';

const ICONS_GALLERY_APP_CONTENT = require('../../templates/apps/icons-gallery.mustache');

/* App Functionality ------------------------------------------------------- */

export const Icons_Gallery = {
    /**
     * Initialises the icons gallery application
     */
    init: () => {
        Icons_Gallery.addToLauncher();
    },

    /**
     * Adds the icons gallery applet to the Launcher
     */
    addToLauncher: () => {
        let callback = () => {
            Icons_Gallery.launch();
        }

        let icons_gallery_tile = new AppTile(
            'Icons Gallery',
            'static/apps/fontawesome.png',
            'static/apps/fontawesome.png',
            callback
        );

        Launcher.registerApplet(icons_gallery_tile);
    },

    /**
     * The core applet launch logic for the applet
     */
    launch: () => {
        WindowManagerInterface.openDefaultWindow('Icons Gallery', ICONS_GALLERY_APP_CONTENT());
    }
}