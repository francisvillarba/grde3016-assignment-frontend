/**
 * aerius/apps/fonts-manager.js
 * 
 * This application allows you to browse through a curated list
 * of fonts that can then be applied to the overall system.
 * 
 * This was primarily added to satisfy the assessment requirements for
 * Curtin University, GRDE3016, Internet Project Development, Assessment 3.
 *
 * @author Francis Villarba <francis.villarba@me.com>
 */

/* Constants ---------------------------------------------------------------- */

import { Launcher, AppTile } from './launcher';
import { WindowManagerInterface } from '../wmi/wmi';

const FONTS_APP_CONTENT = require('../../templates/apps/fonts_manager.mustache');

const DEFAULT_FONT_BUTTON_ID = '#set-default';
const RALEWAY_FONT_BUTTON_ID = '#set-raleway';
const GRANDSTANDER_FONT_BUTTON_ID = '#set-grandstander';
const WORKSANS_FONT_BUTTON_ID = '#set-worksans';

/* App Functionality ------------------------------------------------------- */

export const Fonts_Manager = {
    /**
     * Initialises the fonts browser and configuration application
     */
    init: () => {
        Fonts_Manager.addToLauncher();
    },

    /**
     * Adds the font manager applet to the launcher
     */
    addToLauncher: () => {
        let callback = () => {
            Fonts_Manager.launch();
        }

        let fonts_manager_tile = new AppTile(
            'Fonts Manager',
            'static/apps/google_fonts.png',
            'static/apps/google_fonts.png',
            callback
        );

        Launcher.registerApplet(fonts_manager_tile);
    },

    /**
     * The core applet launch logic for the applet
     */
    launch: () => {
        WindowManagerInterface.openDefaultWindow('Fonts Manager', FONTS_APP_CONTENT());

        // Set the appropriate size for the winodw
        let appID = WindowManagerInterface.getActiveWindow();
        document.querySelector(`div[id='${appID}']`).style.height = '800px';
        document.querySelector(`div[id='${appID}']`).style.width = '400px';

        // Hand off to our core event handler for the rest of the functionality
        handleFontManagerButtonEvents();
    }
};

/* Core Functionality ------------------------------------------------------ */

/**
 * Sets up the event handlers for various buttons in the font manager window
 */
function handleFontManagerButtonEvents() {
    // Default Font --------------------------------------------------------- /

    document.querySelector(DEFAULT_FONT_BUTTON_ID).addEventListener('click', () => {
        // Mouse
        document.body.style.fontFamily = 'sans-serif';
    });
    document.querySelector(DEFAULT_FONT_BUTTON_ID).addEventListener('touchstart', () => {
        // Touch Screen
        document.body.style.fontFamily = 'sans-serif';
    });

    // Raleway Font ---------------------------------------------------------- /

    document.querySelector(RALEWAY_FONT_BUTTON_ID).addEventListener('click', () => {
        // Mouse
        document.body.style.fontFamily = "'Raleway',sans-serif";
    });
    document.querySelector(RALEWAY_FONT_BUTTON_ID).addEventListener('touchstart', () => {
        // Touch Screen
        document.body.style.fontFamily = "'Raleway',sans-serif";
    });

    // Grandstander Font ---------------------------------------------------- /

    document.querySelector(GRANDSTANDER_FONT_BUTTON_ID).addEventListener('click', () => {
        // Mouse
        document.body.style.fontFamily = "'Grandstander', cursive";
    });
    document.querySelector(GRANDSTANDER_FONT_BUTTON_ID).addEventListener('touchstart', () => {
        // Touch Screen
        document.body.style.fontFamily = "'Grandstander', cursive";
    });

    // Worksans Font -------------------------------------------------------- /
    
    document.querySelector(WORKSANS_FONT_BUTTON_ID).addEventListener('click', () => {
        // Mouse
        document.body.style.fontFamily = "'Work Sans', sans-serif";
    });
    document.querySelector(WORKSANS_FONT_BUTTON_ID).addEventListener('touchstart', () => {
        // Touch Screen
        document.body.style.fontFamily = "'Work Sans', sans-serif";
    });
};