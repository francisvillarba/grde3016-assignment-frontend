/**
 * config.js
 * 
 * The core configuration for the ãerius project.
 * Created for GRDE3016 - Internet Project Development, Curtin University,
 * Semester 2, 2020 by Francis Villarba for Assignment 3.
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 */

/**
 * Global Default Configuration
 */
export const DEFAULTS = {
    author: 'Francis Villarba <francis.villarba@me.com>',
    version: 0.5,
}

export const WINDOW_DEFAULTS = {
    // Open, Resizing, Rolled-up, Minimised, Maximised, Exclusive Full-Screen
    state: 1,
    // Is it the currently active window?
    active: true,
    // Can the window be resized?
    resizable: true
}