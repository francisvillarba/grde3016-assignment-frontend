/**
 * The menu bar clock applet that displays the current user's name
 * @author Francis Villarba
 */

// Functionality ------------------------------------------------------------ /

// TODO - Future, add feature to show the user control panel

async function showUsername( name ) {
    let usernameTarget = document.querySelector('#menu-bar-status-user');
    usernameTarget.innerText = name;
}

async function handleUsernameMenuItem() {
    // TODO - This is a stub, put real functionality here
    setTimeout( () => {
        showUsername('Demo User');
    }, 750);
}

// Exports ------------------------------------------------------------------ /
export { handleUsernameMenuItem };