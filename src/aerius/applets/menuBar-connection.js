/**
 * menuBar-connect.js
 * Handles the connection status event
 */

// Functionality ------------------------------------------------------------ /
async function showConnectionStatus( isConnected ) {
    let connectedIndicator = document.querySelector('#menu-bar-status-connected');
    let disconnectedIndicator = document.querySelector('#menu-bar-status-disconnected');

    if( isConnected ) {
        disconnectedIndicator.classList.add('hidden');
        connectedIndicator.classList.remove('hidden');
    } else {
        connectedIndicator.classList.add('hidden');
        disconnectedIndicator.classList.remove('hidden');
    }
}

function showConnectionStatusNotification( isConnected ) {
    // TODO - Finish this implementation stub
}

function handleConnectionStatusEvents() {
    // If offline
    window.addEventListener('offline', () => {
        showConnectionStatus( false );
    });

    // If online
    window.addEventListener('online', () => {
        showConnectionStatus( true );
    });
}

// Exports ------------------------------------------------------------------ /
export { handleConnectionStatusEvents };