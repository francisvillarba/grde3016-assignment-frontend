/**
 * The menu bar clock applet that displays the current time
 * @author Francis Villarba
 */

// Functionality ------------------------------------------------------------ /

async function updateClock() {
    let clockTarget = document.querySelector('#menu-bar-status-clock');
    
    let date = new Date();
    let hours = date.getHours().toLocaleString('en-AU');
    let minutes = date.getMinutes().toLocaleString('en-AU');
    let seconds = date.getSeconds('en-AU');

    // Make sure the time is double digits
    if( hours.toString().length < 2 ) {
        hours = '0' + hours.toString();
    }

    if( minutes.toString().length < 2 ) {
        minutes = '0' + minutes.toString();
    }

    if( seconds.toString().length < 2 ) {
        seconds = '0' + seconds.toString();
    }

    clockTarget.innerText = hours + ':' + minutes + ':' + seconds;
}

async function clock() {
    setTimeout( () => {
        updateClock();
        clock();
    }, 1000);
}

export { clock };