/**
 * menuBar-launcher.js
 * Handles the applet launcher button and invokes the launcher
 * @author Francis Villarba <francis.villarba@me.com>
 */

// Imports ------------------------------------------------------------------ /
import { Launcher } from '../apps/launcher';

// Functionality ------------------------------------------------------------ /

function openLauncher() {
    // Remove the open launcher event
    let launcherButton = document.querySelector('#app-launcher-button');
    launcherButton.removeEventListener( 'click', openLauncher );

    // Open the launcher
    toggleMenuState();
    Launcher.show();

    // Add the close launcher event
    handleCloseLauncherEvent();
}

function handleOpenLauncherEvent() {
    let launcherButton = document.querySelector('#app-launcher-button');
    launcherButton.addEventListener( 'click', openLauncher );
};

function closeLauncher() {
    let launcherButton = document.querySelector('#app-launcher-button');
    launcherButton.removeEventListener( 'click', closeLauncher );

    // Close the launcher
    toggleMenuState();
    Launcher.hide();

    // Add the open launcher event
    handleOpenLauncherEvent();
}

function handleCloseLauncherEvent() {
    let launcherButton = document.querySelector('#app-launcher-button');
    launcherButton.addEventListener( 'click', closeLauncher );
};

function toggleMenuState() {
    let launcherButton = document.querySelector('#app-launcher-button');
    let launcherButtonParent = launcherButton.parentElement;

    if( launcherButton.classList.contains('active')) {
        launcherButton.classList.remove('active');
        launcherButtonParent.removeAttribute('active');
    } else {
        launcherButton.classList.add('active');
        launcherButtonParent.setAttribute('active', true);
    }
};

function handleLauncherButtonEvent() {
    handleOpenLauncherEvent();
};

// Exports ------------------------------------------------------------------ /
export { handleLauncherButtonEvent, openLauncher, closeLauncher };