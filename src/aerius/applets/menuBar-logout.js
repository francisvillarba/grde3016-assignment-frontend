/**
 * menuBar-logout.js
 * 
 * Handles the logout of the user
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 */

// Functionality ------------------------------------------------------------ /

function handleLogoutButton() {
    let logoutButton = document.querySelector('#menu-bar-logout');
    logoutButton.addEventListener('click', () => {
        if( confirm('Are you sure you want to logout?') ){
            // TODO - Currently a stub // Put real logout here!
            window.location.reload();
        }
    });
}

// Exports ------------------------------------------------------------------ /
export { handleLogoutButton };