/**
 * aerius.js
 * 
 * The main entrypoint for the Project Aerius Desktop Experience.
 * Created for GRDE3016 - Internet Project Development, Curtun University,
 * Semester 2, 2020 by Francis Villarba for Assignment 3.
 * 
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 */

// Webpack Compatible Imports ----------------------------------------------- /

import './scss/main.scss';
import './scss/aerius.scss';

// Font awesome compatibility layer with webpack 4
import '../node_modules/@fortawesome/fontawesome-free/css/all.css';
import '../node_modules/@fortawesome/fontawesome-free/js/all.js';


import { Aerius } from './aerius/aerius';
import anime from 'animejs/lib/anime.es';

// Functions ---------------------------------------------------------------- /

/**
 * Initialises the Aerius Desktop Environment
 */
function initAerius() {
    console.log('Aerius', '-', 'Initialising the Aerius Desktop Environment');

    // Set the Loading Text
    document.querySelector('#loader-status').innerHTML = 'Getting ready to launch!';
    
    // Make the app visible
    document.querySelector('#app').classList.remove('hidden');
    document.querySelector('#app-background').classList.remove('hidden');

    // Transition to the app
    anime({
        targets: '#loader',
        keyframes: [
            { opacity: '1', },
            { opacity: '0', left: '100%' }
        ],
        complete: () => {
            document.querySelector('#loader').classList.add('hidden');
            document.querySelector('#app').classList.remove('initial-app-load');
        },
        duration: 1000,
        delay: 500,
        autoplay: true,
        easing: 'easeOutQuad',
    });

    // Hand over control to Aerius
    Aerius.init();
};

document.addEventListener('DOMContentLoaded', () => {
    console.log('Hello World! Project Aerius is heavily in WIP', 'Thanks for checking it out!');
    console.log('Author:', 'Francis Villarba <francis.villarba@me.com>');
});

window.addEventListener('load', () => {
    setTimeout( () => {
        initAerius();
    }, 2100);
});