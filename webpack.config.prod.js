/**
 * webpack.config.prod.js
 * 
 * The production webpack configuration file for IPD Assignment 3
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 */

const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin'); 

const config = {
    mode: "development",
    entry: {
        index: './src/index.js',
        aerius: './src/aerius.js',
    },
    resolve: {
        modules: [
            'node_modules'
        ],
        alias: {
            Templates: path.resolve(__dirname, 'src/templates'),
            Static: path.resolve(__dirname, './static')
        },
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].[hash].bundle.js'
    },
    optimization: {
        splitChunks: {
            chunks: 'async',
            minSize: 20000,
            maxSize: 0,
            minChunks: 1,
            maxAsyncRequests: 30,
            maxInitialRequests: 30,
            automaticNameDelimiter: '~',
            enforceSizeThreshold: 50000,
            cacheGroups: {
                defaultVendors: {
                    test: /[\\/]node_modules[\\/]/,
                    priority: -10
                },
                default: {
                    minChunks: 2,
                    priority: -20,
                    reuseExistingChunk: true
                }
            }
        }
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]',
                        }
                    },
                    {
                        loader: 'image-webpack-loader',
                        options: {
                            bypassOnDebug: false,
                            disable: false,
                            mozjpeg: {
                                progressive: true,
                                quality: 65
                            },
                            optipng: {
                                enabled: false,
                            },
                            pngquant: {
                                quality: [0.65, 0.90],
                                speed: 4
                            },
                            gifsicle: {
                                interlaced: false,
                            },
                        },
                    },
                ],
            },
            {
                test: /\.s(a|c)ss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            esModule: false,
                        },
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: false,
                            modules: false,
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: false,
                            implementation: require('sass'),
                        },
                    },
                ],
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            esModule: false,
                        },
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: false,
                            modules: false,
                        },
                    },
                ]
            },
            {
                test: /\.mustache$/,
                loader: 'mustache-loader?noShortcut',
                options: {
                    minify: true,
                    tiny: false,
                    clientSide: false,
                },
            },
            {
                test: /\.(eot|woff|woff2|ttf)$/,
                use: ['file-loader']
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(),
        new CopyWebpackPlugin({
            patterns: [
                { from: './static', to: 'static' },
                { from: './experiments', to: 'experiments' },
                { from: './.unsplash', to: '.unsplash' },
            ],
        }),
        new HtmlWebpackPlugin({
            template: './src/index.html',
            filename: `index.html`,
            inject: true,
            excludeChunks: [
                'aerius',
            ],
        }),
        new HtmlWebpackPlugin({
            template: './src/aerius.html',
            filename: `aerius.html`,
            inject: true,
            excludeChunks: [
                'index',
            ],
        }),
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id]-[name].css',
        }),
    ]
};

module.exports = config;