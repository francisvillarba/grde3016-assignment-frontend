# IPD Assignment 3 - Project ãerius

  
  
**Frontend**
------
This folder contains front end code for Francis Villarba's Curtin University Assignment for GRDE3016, Internet Project Development (IPD). Created in Semester 2, 2020. 


  

## HOW TO RUN
------
Given the size of the project, there is quite a bit of setup involved before you can run the production version of the code.

To run the code, do the following steps:
1. npm install
2. npm run build:prod
3. npm run start:prod


  
## Documentation
------

Full source documentation in JSDoc3 is available in the docs folder :)
You can launch the documentation in the browser by running 'npm run start:docs'.

Otherwise, all my source code in /src/ is fully documented with source code comments.


  

## Project Specific Commands
------

The following commands can be done to run various functions of the project

| Command | Description |
| ------- | ----------- |
| npm install | Install and resolve all dependencies for the project |
| npm run clean | Cleans the ./dist folder (which contains Webpack 4 output) |
| npm run clean:docs | Cleans the ./docs folder (which contains JSDoc3 output) |
| npm run start:dev | Start the webpack-dev-server using the webpack.config.dev configuration |
| npm run start:prod | Start the production server from ./dist/ |
| npm run start:ios | Start live-server on iOS (via Play.js app) |
| npm build:dev | Build the development version of the project (outputs to ./dist) using Webpack 4 |
| npm run build:prod | Build the production version of the project (outputs to ./dist) using Webpack 4 | 
| npm run build:docs | Build the JSDoc 3 documentation for the project (outputs to ./docs) using JSDoc |

As this project is not an SPA, you can simply run Live-server on the files within ./dist :)


  

Organisation
------

In general, the project has been organised into the following structure

**frontend/**


| Folder | Description |
| ------ | ----------- |
| .vscode | Contains my editor config to keep them synced across devices during development |
| dist | The build output folder from webpack (during development, might not show up in repo) |
| docs | The documentation for the code using JSDoc 3 CLI (during development, might not show up in repo) |
| src | The main source files for the project |
| static | Static assets for when the project is hosted online |
| tests | Test runners and such live here |

---

For more information about particular folders and what they contain, please see the contained README files.

---


  

## Author 
**Created by Francis Villarba** -
francis.villarba@me.com

Curtin University, Semester 2 - 2020.

GRDE3016 - Internet Project Development