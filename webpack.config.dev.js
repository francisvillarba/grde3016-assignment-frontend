/**
 * webpack.config.dev.js
 * 
 * The development webpack configuration file for IPD Assignment 3
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 */

const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin'); 

const config = {
    mode: "development",
    entry: {
        index: './src/index.js',
        aerius: './src/aerius.js',
    },
    resolve: {
        modules: [
            'node_modules'
        ],
        alias: {
            Templates: path.resolve(__dirname, 'src/templates'),
            Static: path.resolve(__dirname, './static')
        },
    },
    // TODO - FUTURE -- Add cache busting auto generated names for production config
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].bundle.js'
    },
    // TODO - FUTURE -- Add performance optimisation for the production config https://webpack.js.org/guides/code-splitting/
    performance: {
        hints: "warning", // enum
        maxAssetSize: 200000, // int (in bytes),
        maxEntrypointSize: 400000, // int (in bytes)
        assetFilter: function (assetFilename) {
            // Function predicate that provides asset filenames
            return assetFilename.endsWith('.css') || assetFilename.endsWith('.js') || assetFilename.endsWith('.scss');
        }
    },
    devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.js$/,
                use: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]',
                        }
                    },
                    {
                        loader: 'image-webpack-loader',
                        options: {
                            bypassOnDebug: true,
                            disable: true,
                            mozjpeg: {
                                progressive: true,
                                quality: 65
                            },
                            optipng: {
                                enabled: false,
                            },
                            pngquant: {
                                quality: [0.65, 0.90],
                                speed: 4
                            },
                            gifsicle: {
                                interlaced: false,
                            },
                        },
                    },
                ],
            },
            {
                test: /\.s(a|c)ss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            esModule: false,
                        },
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                            modules: false,
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                            implementation: require('sass'),
                        },
                    },
                ],
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            esModule: false,
                        },
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                            modules: false,
                        },
                    },
                ]
            },
            {
                test: /\.mustache$/,
                loader: 'mustache-loader?noShortcut',
                options: {
                    // TODO - Future - Make true on production version
                    minify: false,
                    tiny: false,
                    clientSide: false,
                },
            },
            {
                test: /\.(eot|woff|woff2|ttf)$/,
                use: ['file-loader']
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(),
        new CopyWebpackPlugin({
            patterns: [
                { from: './static', to: 'static' },
                { from: './experiments', to: 'experiments' },
                { from: './.unsplash', to: '.unsplash' },
            ],
        }),
        new HtmlWebpackPlugin({
            template: './src/index.html',
            filename: `index.html`,
            inject: true,
            excludeChunks: [
                'aerius',
            ],
        }),
        new HtmlWebpackPlugin({
            template: './src/aerius.html',
            filename: `aerius.html`,
            inject: true,
            excludeChunks: [
                'index',
            ],
        }),
        // TODO - Future - For production webpack plugin, add optimise-css-assets npm module and config
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id]-[name].css',
        }),
    ]
};

module.exports = config;