# Frontend Tests

## Contents

This folder contains the various test runners for the project.

* **Unit**: The suite of unit tests
* **Integration**: The suite of API tests
* **Manual**: The suite of manual tests