/**
 * jsdoc.config.js
 * 
 * Configuration file for the JSDoc documentation system
 * @author Francis Villarba <francis.villarba@student.curtin.edu.au>
 */

module.exports = {
    "plugins": [],
    "recurseDepth": 50,
    "source": {
        "include": [
            "src"
        ],
        "exclude": [
            "dist",
            "node_modules"
        ],
        "includePattern": ".+\\.js(doc|x)?$",
        "excludePattern": "(^|\\/|\\\\)_"
    },
    "sourceType": "module",
    "tags": {
        "allowUnknownTags": true,
        "dictionaries": ["jsdoc", "closure"]
    },
    "templates": {
        "cleverLinks": false,
        "monospaceLinks": false
    },
    "opts": {
        "template": "templates/default",
        "encoding": "utf8",
        "destination": "./docs/",
        "recurse": true
    }
};