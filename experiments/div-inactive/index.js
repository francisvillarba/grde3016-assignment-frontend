/**
 * Window Interactive DOM Element Experiment
 * @author Francis Villarba
 */

/* Globals ----------------------------------------------------------------- */
let windows = [];   // Declare an empty array to store our windows
let activeWindow;   // Declare the currently active window

/* Functions --------------------------------------------------------------- */

function initWindowsArray() {
    let primaryWindow = document.querySelector("[id='2']");
    let secondaryWindow = document.querySelector("[id='1']");

    windows.push(primaryWindow);
    windows.push(secondaryWindow);

    activeWindow = primaryWindow;
};

// Toggles the currently active window
function toggleActive(targetWindow) {
    let target = document.querySelector(`[id='${targetWindow}']`);

    if( target.classList.contains('window-active')) {
        // For debugging
        console.log('ToggleActive', 'Already Active Window, Aborting!');
    } else {
        activeWindow.classList.remove('window-active');
        activeWindow.classList.add('window-inactive');
        target.classList.remove('window-inactive');
        target.classList.add('window-active');

        activeWindow = target;
    }
};

/* Inits ------------------------------------------------------------------- */

window.addEventListener('DOMContentLoaded', () => {
    // Run when the DOM has competely loaded
    initWindowsArray();


    // Let's determine what element was clicked in the dom
    document.onclick = (event) => {
        console.log('Clicked Element', event.target);
        
        let clickTarget = event.target;

        // For Debugging
        console.log('Click Target', clickTarget);
        console.log('Closest Parent', clickTarget.closest('.window-container'));
        console.log('Parent ID', clickTarget.closest('.window-container').id);
        console.log('Parent Window Selector', `[id='${clickTarget.closest('.window-container').id}']`);
        // https://gomakethings.com/a-native-vanilla-javascript-way-to-get-the-closest-matching-parent-element/
        // TODO - Find a way to check if null before proceeding (to stop the bugs!)

        // Logic Psuedo-Code
        // For all events, ensure that the window selected is now primary (and is currently active window) if they clicked a window, otherwise, don't execute anything else
        // If the clickTarget is a close button --> Do Window Close on that ID
        // If the clickTarget is a maximise button --> Do Window maximimise on that ID
        // If the clickTarget is a roll-up button --> Do window rollup

        if( clickTarget.closest('.window-container').id != null ) {
            toggleActive( clickTarget.closest('.window-container').id );
        }
    };


});