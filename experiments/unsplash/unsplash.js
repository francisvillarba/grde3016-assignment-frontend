/**
 * Unpslash API experimentation for the Aerius Desktop Project
 * @author Francis Villarba <francis.villarba@me.com>
 */

/* Globals ----------------------------------------------------------------- */


/* Functionality ----------------------------------------------------------- */

async function getAuthJson() {
    return new Promise( (resolve, reject) => {
        fetch('/.unsplash/authentication.json')
        .then( (res) => res.json() )
        .then( (data) => {
            // For debugging
            // console.log(data);
            // console.log(data['access_key']);
    
            resolve(data['access_key']);
        })
        .catch( (err) => {
            // For debugging
            console.log('An error occured while obtaining authentication file');
            reject(err);
        });
    });
};

async function getRandomWallpaperJson( auth ) {
    return new Promise( (resolve, reject) => {
        fetch(`https://api.unsplash.com/photos/random?client_id=${auth}`)
        .then( (res) => res.json() )
        .then(  (data) => {
            // For debugging
            // console.log(data);

            resolve(data);
        })
        .catch( (err) => {
            // For debugging
            console.log('An error occured while communicating with unsplash');
            reject(err);
        });
    })
};

function setWallpaper( unsplashJson ) {
    // For debugging
    console.log(unsplashJson);

    // Required as part of unsplash attribution guidelines
    const UTM_SOURCE = "?utm_source=Project_ãerius&utm_medium=referral";

    let wallpaperTarget = document.querySelector('#wallpaper');
    let wallpaperAttributionTarget = document.querySelector('#wallpaper-attribution');

    // Clear anything within first
    wallpaperAttributionTarget.innerHTML = '';

    // Declare the fields that we need
    let authorName = unsplashJson.user.name;
    let authorPage = unsplashJson.user.links.html;
    let imageURL = unsplashJson.urls.full;

    // For debugging
    console.log(authorName, authorPage, imageURL);

    // Set the wallpaper
    wallpaperTarget.style.backgroundImage = `url('${imageURL}')`;
    // Create and append attributions
    let headingString = document.createElement('p');
    headingString.innerHTML = 'Background by';
    wallpaperAttributionTarget.appendChild(headingString);

    let authorString = document.createElement('a');
    authorString.setAttribute('href', authorPage + UTM_SOURCE);
    authorString.innerHTML = authorName;
    wallpaperAttributionTarget.appendChild(authorString);

};

function changeWallpaper() {
    // Get authorisation token
    getAuthJson()
    .then( (res) => {
        // For debugging
        // console.log(res);

        // Get wallpaper information from unsplash
        getRandomWallpaperJson( res )
        .then( (res) => {
            // For debugging
            // console.log(res);

            // Set the wallpaper using data from unsplash
            setWallpaper( res );
        })
        .catch( (err) => {
            console.log(err);
        });
    })
    .catch( (err) => {
        console.log(err);
    });
}

/* Events ------------------------------------------------------------------ */

// The main entrypoint for this script
document.addEventListener('DOMContentLoaded', () => {
    let changeWallpaperButton = document.querySelector('#change-wallpaper');
    changeWallpaperButton.addEventListener('click', () => {
        changeWallpaper();
    });
});