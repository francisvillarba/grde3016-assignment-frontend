function init() {
    // Init the button events
    document.querySelector('#open-launcher-button').addEventListener('click', () => {
        let launcherUI = document.querySelector('#launcher-ui');
        launcherUI.classList.remove('hidden');
        launcherUI.classList.add('animate-open');
    });

    document.querySelector('#close-launcher-button').addEventListener('click', () => {
        let launcherUI = document.querySelector('#launcher-ui');
        launcherUI.classList.add('hidden');
        launcherUI.classList.remove('animate-open');
    });
};

document.addEventListener('DOMContentLoaded', () => {
    init();
});