// Initialises our events for this experiment
function initEvents() {
    // Setup the launcher button action
    document.querySelector('#open-launcher-button').addEventListener('click', (event) => {
        event.preventDefault();

        // Open the launcher by unhiding the thing
        document.querySelector('#launcher-ui').classList.remove('hidden');
        document.querySelector('#launcher-ui').classList.add('animate-in');
        // In the final version, this will use template then use anime.js
    });

    document.querySelector('#close-launcher-button').addEventListener('click', (event) => {
        event.preventDefault();

        // Close the launcher by hiding the thing
        document.querySelector('#launcher-ui').classList.add('animate-out');
        document.querySelector('#launcher-ui').classList.remove('animate-in');
        
        // After a delay, add the hidden tag (so it's ready for re-opening)
        setTimeout( () => {
            document.querySelector('#launcher-ui').classList.add('hidden');
            document.querySelector('#launcher-ui').classList.remove('animate-out');
        }, 250)
    });
};

// Runs when the document is ready for action!
document.addEventListener('DOMContentLoaded', () => {
    initEvents();
});