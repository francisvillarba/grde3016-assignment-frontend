/** Desktop Clock Applet --------------------------------------------------- */

async function updateClock() {
    let clockTarget = document.querySelector('#menu-bar-clock');

    let date = new Date();
    let hours = date.getHours().toLocaleString('en-AU');
    let minutes = date.getMinutes().toLocaleString('en-AU');
    let seconds = date.getSeconds('en-AU');

    // For debugging
    // console.log('Current Time', hours, minutes, seconds);

    // Make sure the time is double digit
    if( hours.toString().length < 2 ) {
        hours = '0' + hours.toString();
    }

    if( minutes.toString().length < 2 ) {
        minutes = '0' + minutes.toString();
    }

    if( seconds.toString().length < 2 ) {
        seconds = '0' + seconds.toString();
    }

    clockTarget.innerHTML = hours + ':' + minutes + ':' + seconds;
};

async function clock() {
    setTimeout( () => {
        updateClock();
        clock();
    }, 1000);
};

/** Applet Launcher Menu Icon ---------------------------------------------- */

function toggleMenuState()  {
    let launcherButton = document.querySelector('#app-launcher-button');
    let launcherButtonParent = launcherButton.parentElement;

    if( launcherButton.classList.contains('active')) {
        launcherButton.classList.remove('active');
        launcherButtonParent.removeAttribute('active');
    } else {
        launcherButton.classList.add('active');
        launcherButtonParent.setAttribute('active', true);
    }
};

function handleLauncherButtonEvent() {
    let launcherButton = document.querySelector('#app-launcher-button');
    launcherButton.addEventListener('click', () => {
        toggleMenuState();
    });
};

/** Connection Status Menu Icon -------------------------------------------- */
function showConnectionStatus( isConnected ) {
    let connectedIndicator = document.querySelector('#menu-bar-connected');
    let disconnectedIndicator = document.querySelector('#menu-bar-disconnected');

    if( isConnected ) {
        disconnectedIndicator.classList.add('hidden');
        connectedIndicator.classList.remove('hidden');
    } else {
        disconnectedIndicator.classList.remove('hidden');
        connectedIndicator.classList.add('hidden');
    }
};

function handleConnectionStatusEvent() {
    window.addEventListener('offline', () => {
        showConnectionStatus(false);
    });

    window.addEventListener('online', () => {
        showConnectionStatus(true);
    });
};

/** User Account Menu Item ------------------------------------------------- */

async function showUsername( name ) {
    let usernameTarget = document.querySelector('#menu-bar-user');
    // Technically this is unsafe from a security standpoint, but for the purpose of this demo, it should be fine
    usernameTarget.innerHTML = name;
};

function handleUsernameMenuItem() {
    // TODO - This is a stub, put real functionality here
    setTimeout( () => {
        showUsername('Demo User');
    }, 750);
};

/** Runtime Stuff ---------------------------------------------------------- */

document.addEventListener('DOMContentLoaded', () => {
    clock();
    handleLauncherButtonEvent();
    handleConnectionStatusEvent();
    handleUsernameMenuItem();
});