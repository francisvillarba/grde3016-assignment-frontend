/**
 * Pre-loader experiment for the Aerius Project
 * @author Francis Villarba
 */

function transitionToAppExperience() {
    let loaderContainer = document.querySelector('#loader');

    anime({
        targets: loader,
        keyframes: [
            {
                opacity: '1',
            },
            {
                opacity: '0',
                left: '200%'
            }
        ],
        duration: 1000,
        autoplay: true,
        easing: 'linear',
    });

    document.querySelector('#app').classList.remove('hidden');
    anime({
        targets: '#app',
        keyframes: [
            {
                opacity: '0',
            },
            {
                opacity: '1',
                left: '0px'
            }
        ],
        duration: 750,
        delay: 250,
        autoplay: true,
        easing: 'easeInOutQuad',
    });
};

function setLoadingText() {
    let loadingLabel = document.querySelector('#loader-status');
    loadingLabel.innerHTML = 'Now Loading';
};

function setFinalLoadingText() {
    let loadingLabel = document.querySelector('#loader-status');
    loadingLabel.innerHTML = 'Launching!';
};

// document.addEventListener('DOMContentLoaded', () => {
//     setLoadingText();
//     setTimeout( () => {
//         transitionToAppExperience();
//     }, 5000);
// });

// Use window.onload as this is when everything including all scripts and resources are done loading
// SRC: https://javascript.info/onload-ondomcontentloaded


// Called when the DOM and structure is ready but not all CSS and resources (such as images and icons) are loaded
document.addEventListener('DOMContentLoaded', () => {
    setLoadingText();
    // This is where one should put the pre-load resources calls here
});

// Called when absolutely everything is loaded and ready to go
window.addEventListener('load', () => {
    setLoadingText();

    setTimeout( () => {
        setFinalLoadingText();
        transitionToAppExperience();
    }, 2100);
});