/**
 * Resize window feature experiment for Project Aerius
 * @author Francis Villarba
 */

/* Globals ----------------------------------------------------------------- */

// Globals for the window
let resizeElement;

/* Functions --------------------------------------------------------------- */
function resizeEventHandler(event) {
    window.addEventListener('mousemove', resizeHandler, false);
    window.addEventListener('touchmove', resizeHandler, false);

    window.addEventListener('mouseup', resizeCompleteHandler, false);
    window.addEventListener('touchend', resizeCompleteHandler, false);
};

function resizeHandler(event) {
    event.preventDefault();
    // Setup variables
    let targetWindow = document.querySelector("[id='1']");

    // Debugging
    // console.log('width', targetWindow.offsetWidth);
    // console.log('height', targetWindow.offsetHeight);

    // Handle the dragging
    targetWindow.style.width = (event.clientX - targetWindow.offsetLeft) + 'px';
    targetWindow.style.height = (event.clientY - targetWindow.offsetTop) + 'px';
};

function resizeCompleteHandler(event) {
    event.preventDefault();
    console.log('resize complete fired');

    window.removeEventListener('mousemove', resizeHandler, false);
    window.removeEventListener('touchmove', resizeHandler, false);

    window.removeEventListener('mouseup', resizeCompleteHandler, false);
    window.removeEventListener('touchend', resizeCompleteHandler, false);
};

/* Setup the event handlers ------------------------------------------------ */
window.addEventListener('DOMContentLoaded', () => {
    resizeElement = document.querySelector("[id='1']>.window-footer>.window-footer-right>.footer-shape-resize");
    // For mouse
    resizeElement.addEventListener('mousedown', resizeEventHandler);
    // For touchscreen
    resizeElement.addEventListener('touchstart', resizeEventHandler);
});