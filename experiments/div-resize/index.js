/* DOM Draggable, updated to HTML5 */
// https://www.w3schools.com/howto/howto_js_draggable.asp/
// https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API/


// Globals ------------------------------------------------------------------ /
let titlebarElement;
let windowElement;

// Transformation Globals for the window
let initialMouseX;
let initialMouseY;
let currentTranslateX;
let currentTranslateY;

// Functions ---------------------------------------------------------------- /

function dragHandler(event) {
    let transform =
        'translateX(' + (currentTranslateX - initialMouseX + event.clientX) + 'px)' +
        'translateY(' + (currentTranslateY - initialMouseY + event.clientY) + 'px)';

    // Using style.transform will use 2D graphics GPU instead of standard position variables
    windowElement.style.transform = transform;
};

function dragStartHandler(event) {
    // What to do when we start dragging the window (initially)
    event.dataTransfer.effectAllowed = "copyMove";
    // Get the original mouse co-ordinates
    initialMouseX = event.clientX;
    initialMouseY = event.clientY;

    windowElement.style.opacity = '0.3';

    // Get the initial transformation vector and pull it out
    let currentTransform = window.getComputedStyle(windowElement).getPropertyValue('transform');
    if( currentTransform === null || currentTransform === 'none' ) {
        // The transform has not occured initially
        currentTranslateX = 0;
        currentTranslateY = 0;
    } else {
        // A transform has occured previously
        currentTransform = currentTransform.match(/(-?[0-9\.]+)/g);
        console.log(currentTransform);

        // Set the transform variables
        currentTranslateX = currentTransform[4];
        currentTranslateY = currentTransform[5];

        // For Debugging
        console.log('currentX', currentTranslateX);
        console.log('currentY', currentTranslateY);
    }
};

function dragEndHandler(event) {
    event.preventDefault();
    event.dataTransfer.dropEffect = "move";

    // For debugging
    console.log("Initial Mouse Co-ordinates", initialMouseX, initialMouseY);
    console.log("mouseX: ", event.clientX, windowElement.offsetWidth);
    console.log("mouseY: ", event.clientY, windowElement.offsetHeight);

    // Ensure the final position is set
    windowElement.style.opacity = '1';
};

window.addEventListener('DOMContentLoaded', () => {
    titlebarElement = document.querySelector("[id='1']>.window-titlebar");
    windowElement = document.querySelector("[id='1']");

    windowElement.addEventListener('dragstart', dragStartHandler, false);
    windowElement.addEventListener('drag', dragHandler, false);
    windowElement.addEventListener('dragend', dragEndHandler, false);
    // Webkit requires this to function correctly
    windowElement.addEventListener('dragover', dragEndHandler, false);
});

